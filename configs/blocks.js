const helper = require('../helpers/helper');
/*********
	Get config variable from .env
*********/
var api_public_uri = helper._get(process.env.API_PUBLIC_URI, 'api.v1');
var api_auth_uri = helper._get(process.env.API_AUTH_URI, 'api.v1.auth');

// Configure Required Authentication API
const auth_api = {
	'test' : {
		'alias': 'test',
		'controller': 'app/Controllers/Api/v1/Auth/Test/TestController'
	},
	'test2': {
		'alias': 'test2',
		'controller': 'app/Controllers/Api/v1/Auth/Test/Test2Controller'
	}
};

// Configure Public API
const public_api = {
	'test' : {
		'alias': 'test',
		'controller': 'app/Controllers/Api/v1/Public/Test2/TestController'
	},
	'test2': {
		'alias': 'test2',
		'controller': 'app/Controllers/Api/v1/Public/Test2/Test2Controller'
	}
};

const _blocks = {};
_blocks[api_public_uri] = public_api;
_blocks[api_auth_uri] 	= auth_api;

/****** Export _blocks for Router */
module.exports = _blocks;


/********* Example for results:
**********
**********
	module.exports = {
	    'api.v1.auth': {
			'test' : {
				'alias': 'test',
				'controller': 'app/Controllers/Api/v1/Auth/Test/TestController'
			},
			'test2': {
				'alias': 'test2',
				'controller': 'app/Controllers/Api/v1/Auth/Test/Test2Controller'
			}
		},
		'api.v1': {
			'test' : {
				'alias': 'test',
				'controller': 'app/Controllers/Api/v1/Public/Test2/TestController'
			},
			'test2': {
				'alias': 'test2',
				'controller': 'app/Controllers/Api/v1/Public/Test2/Test2Controller'
			}
		}
	};
**********
**********
*********/
