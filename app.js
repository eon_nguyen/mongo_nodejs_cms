const express = require('express');
const appInit = require('./bin/appInit');

// init application
module.exports = appInit._init(express());
// console.log(`Your port is ${process.env.port}`); // 8626
