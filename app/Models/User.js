//Require Mongoose
var mongoose = require('mongoose');
// var bcrypt = require('bcrypt');
var bcrypt      = require('bcrypt-nodejs');
var Permission  = require('./Permission');

//Define a schema
var UserSchema = new mongoose.Schema({
    username:{ type: String, unique: true, required: true },
    email: { type: String, unique: true, lowercase: true, trim: true, required: true},
    first_name: String,
    last_name: String,
    avatarUrl: String,
    birth_date: String,
    gender: String,
    created_date: String,
    updated_date: String,
    hash_password: { type: String, required: true},
    refresh_token: { type: String },
    created_time_token: { type: String }
}, {
    collection: 'users'
});

UserSchema.methods.check_opt_code = async function(req, res){
    return new Promise((resolve, reject) => {
        setTimeout(function(){
            console.log('xxxxx');
            return resolve('check_opt_code result');
        }, 5000);
    });
};
UserSchema.methods.check_permission = async function(req, res){
    return new Promise((resolve, reject) => {
        var user = req.user;
        var per  = Permission.findOne({"user_id": user._id}, function(err, objResult) {
            if (err) return reject(err);
            return resolve(objResult.status ? true : false);
        });
    });
    // var user = req.user;
    // var cur_per = new Permission({
    //     _id: new mongoose.Types.ObjectId(),
    //     name: 'Knockout.js: Building Dynamic Client-Side Web Applications',
    //     user_id: user._id
    // });
     
    // cur_per.save(function(err) {
    //     if (err) throw err;
     
    //     console.log('Permission successfully saved.');
    // });
};

// comparePassword
UserSchema.methods.comparePassword = function (password) {
    return bcrypt.compareSync(password, this.hash_password)
};

// comparePassword
UserSchema.methods.createHashPassword = function (password) {
    // bcrypt-nodejs is using genSaltSync(), not number (int)10
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
};

//Export function to create "UserSchema" model class
module.exports = mongoose.model('User', UserSchema );
