//Require Mongoose
var mongoose = require('mongoose');

//Define a schema
var PermissionSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    status: Number, 
    user_id: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    }
}, {
    collection: 'permissions'
});

//Export function to create "UserSchema" model class
module.exports = mongoose.model('Permission', PermissionSchema );
