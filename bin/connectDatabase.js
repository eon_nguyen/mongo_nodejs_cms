const mongoose = require('mongoose');
const dbConfig = require('../configs/database');

exports.connectMongoDb = function() {
    //connect to MongoDB
    var mongodbUri = dbConfig.dbUri
    var options = dbConfig.dbOpt;
    mongoose.connect(mongodbUri, options);
};
