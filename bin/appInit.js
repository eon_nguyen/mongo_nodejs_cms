const express = require('express');
const path = require('path');
// const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const database = require('./connectDatabase');

exports._init = function(_app){
	// connect to MongoDB database
	database.connectMongoDb();
	
	// view engine setup
	_app.set('views', path.join(__dirname, '../views'));
	_app.set('view engine', 'ejs');

	// uncomment after placing your favicon in /public
	//_app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
	_app.use(logger('dev'));
	_app.use(bodyParser.json());
	// cookieParse()
	_app.use(bodyParser.urlencoded({ extended: false }));
	_app.use(cookieParser());
	_app.use(express.static(path.join(__dirname, '../public')));

	// Route Group
	var Route = require('../routes/route');
	return Route.defineRouter(_app);
};
