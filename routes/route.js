var express 	 	= require('express');
var routeApi 		= require('./api');
var AuthController  = require('../app/Controllers/AuthController');
var auth 			= require('./api/auth');

var defineRouter = function(_app) {
	// start test express
	var server = _app.listen(3000, function () {
	  	var host = server.address().address;
	  	var port = server.address().port;
	  	console.log("This Node.js Application Is Working At http://%s:%s", host, port)
	});
	// end test express

	// simple logger for this router's requests
	_app.get('/', function (req, res){
		res.send('index');
	});
	_app.use('/auth', auth);
	_app.use('/api/v1', routeApi);
	_app.use('/api/v1/auth', AuthController.isAuthenticated, routeApi);

	// catch 404 and forward to error handler
	_app.use(function(req, res, next) {
	  	var err = new Error('404 Not Found');
	  	err.status = 404;
	  	next(err);
	});

	// error handler
	_app.use(function(err, req, res, next) {
	  	// set locals, only providing error in development
	  	res.locals.message = err.message;
	  	res.locals.error = req.app.get('env') === 'development' ? err : {};

	  	// render the error page
	  	res.status(err.status || 500);
	  	res.render('error');
	});
	return _app;
};

exports.defineRouter = defineRouter;
