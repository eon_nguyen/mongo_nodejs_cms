var _express = require('express');
var _router  = _express.Router();
var _blocks  = require('../configs/blocks.js');
let JsonGenerator  = require('../helpers/json-generator');
var AuthController = require('../app/Controllers/AuthController');

_router.use(async function (req, res, next){
	var baseUrl  = req.baseUrl.replace(/\//g, " ").trim().replace(/\ /g, '.');
	var blocks 	 = _blocks[baseUrl];
	var _path 	 = req._parsedUrl.pathname.replace(/\//g, " ").trim().replace(/\ /g, '/');

	var path_arr = _path.split('/');
	var uri 	 = path_arr[0];
	var block 	 = blocks[uri];
	if (!block){
		next();
		return;
	}
	var alias 		= '/'+block.alias;
	var route_ctrl  = './'+block.controller;
	var controller  = require('../'+route_ctrl);

	// Slug and Capitalize string in function. Exp: test-abc => 'TestAbc' 
	var slug_fn = (path_arr[1] ? path_arr[1] : 'index').split('-').map(function(x){ return x.replace(/^\w/, c => c.toUpperCase()) }).join('');
	var $fn = req.method.toLowerCase() + slug_fn;
	if (req.user){
		/**************
		***************
			waiting for Promise user get permission, if pass then continue, or not abort 404;
			Promise
		**************/
		const hasPer = await req.user.check_permission(req, res);
		if (hasPer){
			if (!controller[$fn])
				next();
			else
				controller[$fn](req, res, next);
		} else
			next();

		/**************
		***************
			waiting for Promise user get opt code check, if pass then continue not abort 404;
			Promise
		*************/
		/*****************
		const hasOtp = await req.user.check_opt_code(req, res);
		if (hasOtp){
			if (!controller[$fn])
				next();
			else
				controller[$fn](req, res, next);
		} else
			next();
		*****************/
	} else {
		if (!controller[$fn])
			next();
		else
			controller[$fn](req, res, next);
	}
});

module.exports = _router;
