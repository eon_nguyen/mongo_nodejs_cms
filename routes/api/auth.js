var express = require('express');
var router = express.Router();

var AuthController = require('../../app/Controllers/AuthController');

// Login
router.post('/login',  AuthController.login);

// Logout
router.post('/logout', AuthController.isAuthenticated, AuthController.logout);

// Token
router.post('/token', AuthController.token);

module.exports = router;
